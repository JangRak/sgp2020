//
//  ViewController.swift
//  SGP_TermProjectApp
//
//  Created by KPUGAME on 2020/06/04.
//  Copyright © 2020 JangRakChoi. All rights reserved.
//

import UIKit

var g_SigunDict = ["가평군": 41820, "고양시": 41280, "과천시": 41290, "광명시": 41210,
    "광주시": 41610, "구리시": 41310, "군포시": 41410, "김포시": 41570,
    "남양주시": 41360, "동두천시": 41250, "부천시": 41190, "성남시": 41130,
    "수원시": 41110, "시흥시": 41390, "안산시": 41270, "안성시": 41550,
    "안양시": 41170, "양주시": 41630, "양평군": 41830, "여주시": 41670,
    "연천군": 41800, "오산시": 41370, "용인시": 41460, "의왕시": 41430,
    "의정부시": 41150, "이천시": 41500, "파주시": 41480, "평택시": 41220,
    "포천시": 41650, "하남시": 41450, "화성시": 41590]

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource
{
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    var pickerDataSource = ["보관소 정보", "자전거 도로 정보", "공공 자전거 대여 정보"]
  
    @IBAction func doneToPickerViewController(segue: UIStoryboardSegue)
    {
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    // 안에 내용물 수
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return pickerDataSource.count
    }
    
    // 해당 데이터
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return pickerDataSource[row]
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.pickerView.delegate = self;
        self.pickerView.dataSource = self;
        
    }


}

