//
//  BikeStorageMapViewController.swift
//  SGP_TermProjectApp
//
//  Created by KPUGAME on 2020/06/11.
//  Copyright © 2020 JangRakChoi. All rights reserved.
//

import UIKit
import MapKit


class BikeStorageMapViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    var Location = CLLocation(latitude: 0.0, longitude: 0.0)
    let regionRadius : CLLocationDistance = 500
    
    var storages : [BikeStorage] = []
    var Storages = NSMutableArray()
    
    func loadInitialData()
    {
        for storage in Storages
        {
            let sTitle = (storage as AnyObject).value(forKey: "BICYCL_DEPOSIT_NM_TITLE") as! NSString as String
            let addr = (storage as AnyObject).value(forKey: "REFINE_LOTNO_ADDR") as! NSString as String
            
            let Lat = Double(((storage as AnyObject).value(forKey: "LAT") as! NSString as String).trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted))!
            
            let Logt = Double(((storage as AnyObject).value(forKey: "LOGT") as! NSString as String).trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted))!
            
            let storage = BikeStorage(title: sTitle, locationName: addr, coordinate: CLLocationCoordinate2D(latitude: Lat, longitude: Logt))
            
            storages.append(storage)
        }
    }
    
    private func mapView(_ mapView: MKMapView, annotationView view : MKAnnotationView, calluotAccessoryControlTapped control : UIControl){
        let location = view.annotation as! BikeStorage
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation)->MKAnnotationView?{
             // 2. 이 주석(annotation)이 Artwork 객체인지 확인!!
             // 그렇지 않으면 nil, 지도 뷰에서 기본 주석 뷰를 사용하도록 돌아감.
             /// guard let : if let과 비슷, assure로 확인하는 것임. nil이 아니면 assign, nil이면 return
              print("succeed2")
             guard let annotation = annotation as? BikeStorage else {return nil}
             print("succeed")
             // 3. 마커가 나타나도록 MKMarkerAnnotationView를 만듦.
             /// 이 자습서 뒷부분에서는 MKAnnotationView 마커 대신 이미지를 표시하는 객체를 만듫ㅁ
             let identifier = "marker"
             var view: MKMarkerAnnotationView
     
             // 4. 코드를 새로 생성하기 전, 재사용 가능한 주석 뷰를 사용할 수 있는지 확인
             if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                 as? MKMarkerAnnotationView{
                 dequeuedView.annotation = annotation
                 view = dequeuedView
             }else{
                 view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                 view.canShowCallout = true
                 view.calloutOffset = CGPoint(x: -5, y: 5)
                 view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
     
             }
             return view
         }
    
    func centerMapOnLocation(location: CLLocation)
    {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        centerMapOnLocation(location: Location)
        
        mapView.delegate = self
        loadInitialData()
        mapView.addAnnotations(storages)
        // Do any additional setup after loading the view.
    }
    

   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
