//
//  BikeRoadTableViewController.swift
//  SGP_TermProjectApp
//
//  Created by KPUGAME on 2020/06/04.
//  Copyright © 2020 JangRakChoi. All rights reserved.
//

import UIKit
import Speech


class BikeStorageTableViewController: UITableViewController, XMLParserDelegate, UISearchBarDelegate {

    @IBOutlet weak var myTextView: UITextView!
    @IBOutlet var tbData: UITableView!
    @IBOutlet weak var transcribeButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet var searchFooter: SearchFooter!
    
    
    //SearchController
    let searchController = UISearchController(searchResultsController: nil)
    var filteredElements = NSMutableArray()
    
    //Audio
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "ko-KR"))!
    private var speechRecognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var speechRecognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    var isNoResult : Bool = false
    
    //xml파일 을 다운로드 및 파싱하는 오브젝트
    var parser = XMLParser()
    //피드 데이터를 저장하는 뮤터블 어레이
    var posts = NSMutableArray()
    // 타이틀과 데이트 같은 피드데이트를 저장하는 뮤터블 딕셔너리
    var elements = NSMutableDictionary()
    var element = NSString()
    
    //저장소 이름
    var storageNm = NSMutableString()
    // 지번주소
    var addr = NSMutableString()
    
    //경도, 위도
    var Xpos =  NSMutableString()
    var Ypos = NSMutableString()
    // 전화번호
    var telNo = NSMutableString()
    // 설치형태
    var storageType = NSMutableString()
    // 공기주입기비치여부
    var isAirPushable = NSMutableString()
    // 보관대수
    var numOfStorages = NSMutableString()
    // 데이터갱신일
    var dateOfRefresh = NSMutableString()

    var xmlURL =  "https://openapi.gg.go.kr/BICYCLDEPOSIT?KEY=837c6677d0794a89bb57ca8a0c6dd523&pIndex=1&pSize=350&SIGUN_CD="
 
    func isFiltering() -> Bool
    {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        
        return searchController.isActive && (!searchBarIsEmpty() || searchBarScopeIsFiltering)
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText( _searchText: String, scope : String = "All")
    {
        filteredElements = []
        
        for object in posts
        {
 
            if searchBarIsEmpty() && scope == "All"
            {
                filteredElements.add(object)
            }
            else
            {
                var isInSearchText = ((object as AnyObject).value(forKey: "REFINE_LOTNO_ADDR") as! NSString as String).lowercased().contains(_searchText.lowercased())
                if scope == "All"
                {
                    if (isInSearchText)
                    {
                        filteredElements.add(object)
                    }
                }
                else
                {
                    if searchBarIsEmpty()
                    {
                        isInSearchText = true
                    }
                    if (isInSearchText && ((object as AnyObject).value(forKey: "AIR_INJEK_POSTNG_YN") as! NSString as String).lowercased().contains("y"))
                    {
                        filteredElements.add(object)
                    }
                }
            }
        }
        
        tbData!.reloadData()
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes atrributeDict: [String : String])
    {
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "row")
        {
            elements = NSMutableDictionary()
            elements = [:]
            storageNm = NSMutableString()
            storageNm = ""
            addr = NSMutableString()
            addr = ""
            
            //위도 경도
            Xpos = NSMutableString()
            Xpos = ""
            Ypos = NSMutableString()
            Ypos = ""
            
            telNo = NSMutableString()
            telNo = ""
            // 설치형태
            storageType = NSMutableString()
            storageType = ""
            // 공기주입기비치여부
            isAirPushable = NSMutableString()
            isAirPushable = ""
            // 보관대수
            numOfStorages = NSMutableString()
            numOfStorages = ""
            
            dateOfRefresh = NSMutableString()
            dateOfRefresh = ""
        }
    }
   
    func parser(_ parser: XMLParser, foundCharacters string: String){
        if element.isEqual(to: "BICYCL_DEPOSIT_NM_TITLE"){
            storageNm.append(string)
        }else if element.isEqual(to: "REFINE_LOTNO_ADDR"){
            addr.append(string)
        }
        else if element.isEqual(to: "LAT"){
            Xpos.append(string)
        }
        else if element.isEqual(to: "LOGT"){
            Ypos.append(string)
        }
        else if element.isEqual(to: "MNGINST_TELNO"){
            telNo.append(string)
        }
        else if element.isEqual(to: "INSTL_DIV_NM"){
            storageType.append(string)
        }
        else if element.isEqual(to: "AIR_INJEK_POSTNG_YN"){
            isAirPushable.append(string)
        }
        else if element.isEqual(to: "CUSTODY_CNT"){
            numOfStorages.append(string)
        }
        else if element.isEqual(to: "DATA_STD_DE"){
            dateOfRefresh.append(string)
        }
    }
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?)
    {
        if( elementName as NSString).isEqual(to: "row"){
            if !storageNm.isEqual(nil){
                elements.setObject(storageNm, forKey: "BICYCL_DEPOSIT_NM_TITLE" as  NSCopying)
            }
            
            if !addr.isEqual(nil){
                elements.setObject(addr, forKey: "REFINE_LOTNO_ADDR" as NSCopying)
            }
            if !Xpos.isEqual(nil){
                           elements.setObject(Xpos, forKey: "LAT" as NSCopying)
                       }
            if !Ypos.isEqual(nil){
                           elements.setObject(Ypos, forKey: "LOGT" as NSCopying)
                       }
            if !telNo.isEqual(nil){
                                      elements.setObject(telNo, forKey: "MNGINST_TELNO" as NSCopying)
                                  }
            if !storageType.isEqual(nil){
                                      elements.setObject(storageType, forKey: "INSTL_DIV_NM" as NSCopying)
                                  }
            if !isAirPushable.isEqual(nil){
                                      elements.setObject(isAirPushable, forKey: "AIR_INJEK_POSTNG_YN" as NSCopying)
                                  }
            if !numOfStorages.isEqual(nil){
                                      elements.setObject(numOfStorages, forKey: "CUSTODY_CNT" as NSCopying)
                                  }
            if !dateOfRefresh.isEqual(nil){
                elements.setObject(dateOfRefresh, forKey: "DATA_STD_DE" as NSCopying)
            }
            
            posts.add(elements)
        }
    }
    func beginParsing()
    {
        let curText = myTextView.text!
        if let entry = g_SigunDict[curText]
        {
            let curURL = xmlURL + String(entry)
            posts = []
            parser = XMLParser(contentsOf:(URL(string:curURL))!)!
            parser.delegate = self
            parser.parse()
            tbData!.reloadData()
        }
        else
        {
            myTextView.text = "검색결과 없음"
            posts = []
            parser.parse()
            tbData!.reloadData()
            isNoResult = true
        }
        
    }
    
    @IBAction func startTranscribing(_ sender: Any) {
        transcribeButton.isEnabled = false
        stopButton.isEnabled = true
        try! startSession()
        isNoResult = false
    }
    @IBAction func stopTranscribing(_ sender: Any) {
        if audioEngine.isRunning {
            audioEngine.stop()
            speechRecognitionRequest?.endAudio()
            transcribeButton.isEnabled = true
            stopButton.isEnabled = false
            beginParsing()
        }
     
        
     
    }
    
    func authorizeSR()
    {
           SFSpeechRecognizer.requestAuthorization
            {
                authStatus in OperationQueue.main.addOperation
                {
                    switch authStatus
                    {
                        case .authorized:
                            
                            self.transcribeButton.isEnabled = true
                        case .denied:
                            self.transcribeButton.isEnabled = false
                            self.transcribeButton.setTitle("Speech recognition access denied by user", for: .disabled)
                            
                        case .restricted:
                            self.transcribeButton.isEnabled = false
                            self.transcribeButton.setTitle("Speech recognition restricted on device", for: .disabled)
                            
                        case .notDetermined:
                            self.transcribeButton.isEnabled = false
                            self.transcribeButton.setTitle("Speech recognition not authorized", for: .disabled)
                    @unknown default:
                        print("authorize Error")
                    }
               }
           }
    }
       
    
    func startSession() throws {
        if let recognitionTask = speechRecognitionTask {
            recognitionTask.cancel()
            self.speechRecognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(AVAudioSession.Category.record)
        
        speechRecognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let recognitionRequest = speechRecognitionRequest else
        { fatalError("SFSpeechAudioBufferRecognitionRequest object creation failed") }
        
        let inputNode = audioEngine.inputNode
        
        recognitionRequest.shouldReportPartialResults = true
        
        speechRecognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest)
        {
            result, error in
            
            var finished = false
            
            if let result = result
            {
                //여기다가 textview수정 코드 삽입
                if !self.isNoResult
                {
                    self.myTextView.text = result.bestTranscription.formattedString
                }
                finished = result.isFinal
            }
            if error != nil || finished
            {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.speechRecognitionRequest = nil
                self.speechRecognitionTask = nil
                
                self.transcribeButton.isEnabled = true
            }
        }
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) {
            (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.speechRecognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        try audioEngine.start()
    }
    
    @IBAction func doneToBikeStorageViewController(segue: UIStoryboardSegue)
    {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueToBikeStorageDetail"
        {
            var curPost = NSMutableArray()
            if isFiltering()
            {
                curPost = filteredElements
            }
            else
            {
                curPost = posts
            }

            if let cell = sender as? UITableViewCell
            {
                let indexPath = tableView.indexPath(for: cell)
                let StorageName = (curPost.object(at: (indexPath?.row)!)as AnyObject).value(forKey:"BICYCL_DEPOSIT_NM_TITLE") as! NSString as String
                let Addr = (curPost.object(at: (indexPath?.row)!)as AnyObject).value(forKey:"REFINE_LOTNO_ADDR") as! NSString as String
                let TelNo = (curPost.object(at: (indexPath?.row)!)as AnyObject).value(forKey:"MNGINST_TELNO") as! NSString as String
                let StorageType = (curPost.object(at: (indexPath?.row)!)as AnyObject).value(forKey:"INSTL_DIV_NM") as! NSString as String
                let IsAirPushable = (curPost.object(at: (indexPath?.row)!)as AnyObject).value(forKey:"AIR_INJEK_POSTNG_YN") as! NSString as String
                let NumOfStorages = (curPost.object(at: (indexPath?.row)!)as AnyObject).value(forKey:"CUSTODY_CNT") as! NSString as String
                let DateOfRefresh = (curPost.object(at: (indexPath?.row)!)as AnyObject).value(forKey:"DATA_STD_DE") as! NSString as String
                
                let Lat = Double(((curPost.object(at: (indexPath?.row)!)as AnyObject).value(forKey:"LAT") as! NSString as String).trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted))!
                
                let Logt = Double(((curPost.object(at: (indexPath?.row)!)as AnyObject).value(forKey:"LOGT") as! NSString as String).trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted))!
                
                
                if let nav = segue.destination as? UINavigationController
                {
                    let Posts : [String] = [StorageName, Addr, TelNo, StorageType, IsAirPushable, NumOfStorages, DateOfRefresh]
                    
                    if let detailHospitalTableViewController = nav.topViewController as? BikeStorageDetailViewController
                    {
                        detailHospitalTableViewController.posts = Posts
                        detailHospitalTableViewController.posX = Lat
                        detailHospitalTableViewController.posY = Logt
                        detailHospitalTableViewController.Storages = posts
                    }
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stopButton.isEnabled = false
        authorizeSR()
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Detail Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        searchController.searchBar.scopeButtonTitles = ["All", "isAirPushable"]
        searchController.searchBar.delegate = self
        
        tableView.tableFooterView = searchFooter
        
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if isFiltering()
        {
            searchFooter.setIsFilteringToShow(filteredItemCount: filteredElements.count, of: posts.count)
            return filteredElements.count
        }
        searchFooter.setNotFiltering()
        return posts.count
    }
    //테이블뷰 셀의 내용은 타이틀과 서브타이틀을 포스트 배열 원소에서 yadmNm과 addr에해당하는 value로설정
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        if isFiltering()
        {
            
            cell.textLabel?.text = (filteredElements.object(at: indexPath.row) as AnyObject
                ).value(forKey: "BICYCL_DEPOSIT_NM_TITLE") as! NSString as String
            
            cell.detailTextLabel?.text = (filteredElements.object(at: indexPath.row) as AnyObject
                ).value(forKey: "REFINE_LOTNO_ADDR") as! NSString as String
            // Configure the cell...
            
            return cell
        }
            
        else
        {
            
            cell.textLabel?.text = (posts.object(at: indexPath.row) as AnyObject
                ).value(forKey: "BICYCL_DEPOSIT_NM_TITLE") as! NSString as String
            
            cell.detailTextLabel?.text = (posts.object(at: indexPath.row) as AnyObject
                ).value(forKey: "REFINE_LOTNO_ADDR") as! NSString as String
            // Configure the cell...
            
            return cell
        }
    }
    
    
    
}
extension BikeStorageTableViewController : UISearchResultsUpdating
{
    func updateSearchResults(for searchController : UISearchController)
    {
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(_searchText: searchController.searchBar.text!, scope: scope)
    }
    
}

extension BikeStorageTableViewController
{
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope : Int)
    {
        filterContentForSearchText(_searchText: searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
   
}

extension BikeStorageTableViewController
{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        print("Succeeded")
        //searchBar.resignFirstResponder()
        if let entry = g_SigunDict[searchController.searchBar.text!]
        {
            let curURL = xmlURL + String(entry)
            posts = []
            parser = XMLParser(contentsOf:(URL(string:curURL))!)!
            parser.delegate = self
            parser.parse()
            searchBar.text! = ""
            tbData!.reloadData()
        }
    }
}
